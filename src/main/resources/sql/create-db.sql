drop schema public cascade; 

drop table uzytkownik if exists;

create table uzytkownik (
uz_id integer not null primary key,
imie varchar(50),
nazwisko varchar(50),
opis varchar(200),
data_dolaczenia date,
login varchar(25),
haslo varchar(25),
);

drop table follower if exists;

create table follower (
follower_id integer not null primary key,
nickname varchar(50)
);

drop table wpis if exists;

create table wpis (
nr_wpisu_id integer not null primary key,
autor_login varchar(50),
tresc  varchar(280),
data_wpisu date
);

