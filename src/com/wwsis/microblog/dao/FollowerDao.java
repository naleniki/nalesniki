package com.wwsis.microblog.dao;
import WWSIS.Microblog.model.*;
public interface FollowerDao {

	void dodanie_obserwowania(String nickname);
    void usuniecie_obserwowania(String nickname);
    boolean sprawdzenie_follower(String nickname);
	
}
