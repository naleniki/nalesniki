package com.wwsis.microblog.dao;

import java.util.List;

import WWSIS.Microblog.model.Wpis;

public interface WiadomoscDao {
    List<Wpis> timeline(String imie, String nazwisko);
    List<Wpis> full_timeline(String login, String nickname);
    List<Wpis> full_public_timeline();
    String dodaj_wiadomosc(String imie, String nazwisko, String tresc);
}
