package com.wwsis.microblog.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import com.wwsis.microblog.dao.UzytkownikDao;

import WWSIS.Microblog.model.Uzytkownik;

public class UzytkownikDaoImpl implements UzytkownikDao{

	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public Uzytkownik pobranie_uzytkownika(String login) {
		Query Uzytkownik = entityManager.createQueary("SELECT Uzytkownik FROM Uzytkownik, Uzytkownik a WHERE a.login= :login");
		return Uzytkownik;
	}

	@Override
	public Uzytkownik dodanie_uzytkownika(String imie1,
			String nazwisko1,
			String opis1,
			String login1,
			String haslo1) {
		
		Query Uzytkownik = ("INSERT INTO Uzytkownik (imie, nazwisko, opis, login, haslo) values (imie1, nazwisko1, opis1, login1, haslo)").setParameter(1, imie1).setParameter(2, nazwisko1).setParameter(3, opis1).setParameter(4, login1).setParameter(1, haslo1).executeUpdate();
		return Uzytkownik;
	}

}
