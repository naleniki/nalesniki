package com.wwsis.microblog.dao;
import java.util.List;

import WWSIS.Microblog.model.*;

public interface UzytkownikDao {
     Uzytkownik pobranie_uzytkownika(String login);
     Uzytkownik dodanie_uzytkownika(String imie,
		String nazwisko,
		String opis,
		String login,
		String haslo);
      
}
