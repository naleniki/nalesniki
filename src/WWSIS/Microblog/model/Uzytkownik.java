package WWSIS.Microblog.model;
import javax.persistence.Entity;

@Entity
@Table (name= "uzytkownik" )
public
class Uzytkownik {


        @Column(name = "uz_id", nullable = false)
        private int uz_id;

        @Column(name = "imie")
        private String imie;

        @Column(name = "nazwisko")
        private String nazwisko;

        @Column(name = "opis")
        private String opis;

        @Column(name = "data_dolaczenia")
        private String data_dolaczenia;

        @Column(name = "login")
        private String login;

        @Column(name = "haslo")
        private String haslo;



        public int getuz_id(){ return uz_id; }
        public void setuz_id(int uz_id){
                this.uz_id = uz_id;
        }
        public String getimie(){ return imie; }
        public void setimie(String imie) {
                this.imie=imie;
        }
        public String getnazwisko(){ return nazwisko; }
        public void setnazwisko(String nazwisko) {
                this.nazwisko = nazwisko;
        }
        public String getopis(){ return opis; }
        public void setopis(String opis) {
                this.opis = opis;
        }
        public String getdata_dolaczenia(){ return data_dolaczenia; }
        public void setdata_dolaczenia(String data_dolaczenia) {
                this.data_dolaczenia = data_dolaczenia;
        }
        public String getlogin(){ return login; }
        public void setlogin(String login) {
                this.login = login;
        }
        public String gethaslo(){ return haslo; }
        public void sethaslo(String haslo) {
                this.haslo = haslo;
        }


}