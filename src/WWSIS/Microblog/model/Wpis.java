package WWSIS.Microblog.model;
import javax.persistence.Entity;
@Entity
@Table (name= "wpis" )
public class Wpis {


        @Column(name = "nr_wpisu_id", nullable = false)
        private int nr_wpisu_id;

        @Column(name = "autor_login")
        private String autor_login;

        @Column(name = "tresc")
        private String tresc;

        @Column(name = "data_wpisu")
        private String data_wpisu;


        public int getnr_wpisu_id(){ return nr_wpisu_id; }
        public void setnr_wpisu_id(int nr_wpisu_id){
                this.nr_wpisu_id = nr_wpisu_id;
        }
        public String getautor_login(){ return autor_login; }
        public void setautor_login(String autor_login) {
                this.autor_login = autor_login;
        }
        public String gettresc(){ return tresc; }
        public void settresc(String tresc) {
                this.tresc = tresc;
        }
        public String getdata_wpisu(){ return data_wpisu; }
        public void setdata_wpisu(String data_wpisu) {
                this.data_wpisu = data_wpisu;
        }
}