package WWSIS.Microblog.model;
import javax.persistence.Entity;
@Entity
@Table (name= "follower" )
class Follower {


    @Column(name = "follower_id", nullable = false)
    private int follower_id;

    @Column(name = "nickname")
    private String nickname;


    public int getfollower_id(){ return follower_id; }
    public void setfollower_id(int follower_id){
        this.follower_id = follower_id;
    }
    public String getnickname(){ return nickname; }
    public void setnickname(String nickname) {
        this.nickname = nickname;
    }
}